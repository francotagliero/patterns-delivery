FROM gradle:7.2.0-jdk11 AS builder
WORKDIR /app
COPY --chown=gradle:gradle . .
RUN gradle assemble --no-daemon

# Etapa de tiempo de ejecución
FROM adoptopenjdk:11-jre-hotspot
COPY --from=builder /app/build/libs/patterns-delivery.jar /app/app.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/app/app.jar"]