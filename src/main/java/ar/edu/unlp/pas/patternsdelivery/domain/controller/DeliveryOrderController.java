/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ar.edu.unlp.pas.patternsdelivery.domain.controller;

import ar.edu.unlp.pas.patternsdelivery.domain.dto.DeliveryOrderDTO;
import ar.edu.unlp.pas.patternsdelivery.domain.service.DeliveryOrderService;
import ar.edu.unlp.pas.patternsdelivery.domain.model.*;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Controller for managing delivery orders.
 */

@RestController
@RequestMapping("/api/v1/delivery")
public class DeliveryOrderController {
    private final DeliveryOrderService deliveryOrderService;


    @Autowired
    public DeliveryOrderController(DeliveryOrderService deliveryOrderService) {
        this.deliveryOrderService = deliveryOrderService;
    }

    // Constructor and autowired fields
    
    /**
     * Create a new delivery order.
     * 
     * @param deliveryOrder The delivery order to create.
     * @return The created delivery order.
     */
    @PostMapping("/checkout")
    @ApiOperation(value = "Create a new delivery order")
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Delivery order created successfully"),
        @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<DeliveryOrderDTO> createDeliveryOrder(@RequestBody DeliveryOrder deliveryOrder) {
        return ResponseEntity.ok(deliveryOrderService.createNew(deliveryOrder));
    }
    
    /**
     * Get a delivery order by ID.
     * 
     * @param id The ID of the delivery order.
     * @return The delivery order.
     */
    @GetMapping("/{id}")
    @ApiOperation(value = "Get a delivery order by ID")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Delivery order found"),
        @ApiResponse(code = 404, message = "Delivery order not found")
    })
    public ResponseEntity<DeliveryOrder> getDeliveryOrderById(@PathVariable("id") Long id) {
        DeliveryOrder order = deliveryOrderService.getDeliveryOrderById(id);
        if (order != null) {
            return ResponseEntity.ok(order);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    
     
    /**
     * Get pending delivery orders.
     * 
     * @return A list of pending delivery orders.
     */
    @GetMapping
    @ApiOperation(value = "Get pending delivery orders")
    @ApiResponse(code = 200, message = "List of pending delivery orders")
    public ResponseEntity<List<DeliveryOrderDTO>> getPendingDeliveryOrders() {
        List<DeliveryOrderDTO> orders = deliveryOrderService.getPendingDeliveryOrders();
        return ResponseEntity.ok(orders);
    }

    @PutMapping("/{orderId}/deliverer")
    @ApiOperation(value = "Set deliverer for delivery order")
    public ResponseEntity<DeliveryOrderDTO> setDelivererForOrder(@PathVariable("orderId") long orderId) {
        return ResponseEntity.ok(deliveryOrderService.setDelivererForOrder(orderId));
    }

    @PutMapping("/{orderId}/delivered")
    public ResponseEntity<DeliveryOrderDTO> delivered(@PathVariable("orderId") long orderId) {
        return ResponseEntity.ok(deliveryOrderService.delivered(orderId));
    }

    @PutMapping("/{orderId}/cancelled")
    public ResponseEntity<DeliveryOrderDTO> cancelled(@PathVariable("orderId") long orderId) {
        return ResponseEntity.ok(deliveryOrderService.cancelled(orderId));
    }


    /**
     * Exception handler for handling exceptions in the controller.
     * 
     * @param exception The exception to handle.
     * @return A response entity with an error message.
     */
    @ExceptionHandler
    public ResponseEntity<String> handleException(Exception exception) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(exception.getMessage());
    }
}