/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ar.edu.unlp.pas.patternsdelivery.infrastructure;

import ar.edu.unlp.pas.patternsdelivery.domain.service.DeliveryOrderService;
import ar.edu.unlp.pas.patternsdelivery.domain.model.DeliveryData;
import ar.edu.unlp.pas.patternsdelivery.domain.model.DeliveryOrder;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.*;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;
import org.springframework.beans.factory.annotation.Autowired;


/**
 *
 * @author RodAlejo
 */

public class DeliveryOrderConsumer {
    
    private static DeliveryOrderService deliveryOrderService;

    @Autowired
    public DeliveryOrderConsumer(DeliveryOrderService deliveryOrderServiceA) {
        deliveryOrderService = deliveryOrderServiceA;
    }
    
    private final static String QUEUE_NAME = "cartItemsQueue";

    public static void start() throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME, true, false, false, null);

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), StandardCharsets.UTF_8);

            try {
                ObjectMapper objectMapper = new ObjectMapper();
                DeliveryData deliveryData = objectMapper.readValue(message, DeliveryData.class);
                processDeliveryOrder(deliveryData);
            } catch (Exception e) {
                e.printStackTrace();
            }
        };

        // Iniciar la recepci?n de mensajes
        channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> {
        });
    }

    private static void processDeliveryOrder(DeliveryData deliveryData) {
        DeliveryOrder deliveryOrder = new DeliveryOrder();
        System.out.println("Data: "+ deliveryData.getProductId());
        
        // Implementar creaci?n del deliveryOrder 
        
        //deliveryOrderService.createDeliveryOrder(deliveryOrder);
        
    }
}