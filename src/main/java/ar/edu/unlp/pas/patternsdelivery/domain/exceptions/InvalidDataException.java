package ar.edu.unlp.pas.patternsdelivery.domain.exceptions;

public class InvalidDataException extends RuntimeException{
    public InvalidDataException(final String message) {
        super(message);
    }
}
