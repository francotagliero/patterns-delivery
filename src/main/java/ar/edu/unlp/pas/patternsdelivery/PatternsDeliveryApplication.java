package ar.edu.unlp.pas.patternsdelivery;

import ar.edu.unlp.pas.patternsdelivery.infrastructure.DeliveryOrderConsumer;
import java.io.IOException;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
@SpringBootApplication
public class PatternsDeliveryApplication {

	public static void main(String[] args) {
        SpringApplication.run(PatternsDeliveryApplication.class, args);
	}

}
