/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ar.edu.unlp.pas.patternsdelivery.domain.repository;

import ar.edu.unlp.pas.patternsdelivery.domain.model.DeliveryOrder;
import ar.edu.unlp.pas.patternsdelivery.domain.model.status.OrderStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 *
 * @author RodAlejo
 */
@Repository
public interface DeliveryOrderRepository extends JpaRepository<DeliveryOrder, Long> {

    Optional<DeliveryOrder> findOrderById(long id);

    List<DeliveryOrder> findByStatus(OrderStatus orderStatus);
    
}
