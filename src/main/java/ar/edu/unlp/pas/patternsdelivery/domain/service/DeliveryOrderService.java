/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ar.edu.unlp.pas.patternsdelivery.domain.service;

import ar.edu.unlp.pas.patternsdelivery.domain.dto.DeliveryOrderDTO;
import ar.edu.unlp.pas.patternsdelivery.domain.dto.PersonDTO;
import ar.edu.unlp.pas.patternsdelivery.domain.dto.PublicationDTO;
import ar.edu.unlp.pas.patternsdelivery.domain.exceptions.InvalidDataException;
import ar.edu.unlp.pas.patternsdelivery.domain.exceptions.ResourceNotFoundException;
import ar.edu.unlp.pas.patternsdelivery.domain.model.DeliveryOrder;
import ar.edu.unlp.pas.patternsdelivery.domain.model.status.OrderStatus;
import ar.edu.unlp.pas.patternsdelivery.domain.repository.DeliveryOrderRepository;
import ar.edu.unlp.pas.patternsdelivery.domain.utils.CollectionUtils;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class DeliveryOrderService {
    private static final String DELIVERY_ORDER = "Delivery order";


    private final DeliveryOrderRepository deliveryOrderRepository;
    private final PersonService personService;
    private final PublicationService publicationService;

    private DeliveryOrderDTO getDeliveryOrderDTO(DeliveryOrder deliveryOrder) {
        PersonDTO buyer = personService.getPerson(deliveryOrder.getBuyerId());
        if (!this.personService.validateAddressForPerson(buyer, deliveryOrder.getAddressId())) {
            throw new InvalidDataException("Address doesnt belong to the seller.");
        }

        DeliveryOrderDTO deliveryOrderDTO = deliveryOrder.toDTO();
        deliveryOrderDTO.setAddress(buyer.getShippingAddress().stream().filter(a -> a.getId() == deliveryOrder.getAddressId()).findFirst().orElseThrow());
        deliveryOrderDTO.setBuyer(buyer);

        PublicationDTO publication = publicationService.getPublication(deliveryOrder.getPublicationId());
        deliveryOrderDTO.setPublication(publication);

        if (deliveryOrder.getDelivererId() != null) {
            PersonDTO deliverer = personService.getPerson(deliveryOrder.getBuyerId());
            deliveryOrderDTO.setDeliverer(deliverer);
        }

        return deliveryOrderDTO;
    }

    public DeliveryOrderDTO createNew(DeliveryOrder deliveryOrder) {
        deliveryOrder.setStatus(OrderStatus.STATUS_PENDING);
        DeliveryOrderDTO deliveryOrderDTO = this.getDeliveryOrderDTO(deliveryOrder);
        this.save(deliveryOrder);
        deliveryOrderDTO.setId(deliveryOrder.getId());
        return deliveryOrderDTO;
    }

    private List<DeliveryOrder> getAllPending() {
        return this.deliveryOrderRepository.findByStatus(OrderStatus.STATUS_PENDING);
    }

    public DeliveryOrder getDeliveryOrderById(Long id) {
        return deliveryOrderRepository.findById(id).orElse(null);
    }

    public DeliveryOrder save(DeliveryOrder deliveryOrder) {
        return deliveryOrderRepository.save(deliveryOrder);
    }

    public DeliveryOrder getById(long id) {
        return deliveryOrderRepository.findOrderById(id)
                .orElseThrow(() -> new ResourceNotFoundException(DELIVERY_ORDER, "id", id));
    }

    public List<DeliveryOrderDTO> getPendingDeliveryOrders() {
        List<DeliveryOrder> pendingDeliveryOrders = this.getAllPending();
        return CollectionUtils.mapList(pendingDeliveryOrders, this::getDeliveryOrderDTO);
    }

    public DeliveryOrderDTO setDelivererForOrder(long orderId) {
        DeliveryOrder deliveryOrder = this.getById(orderId);
        PersonDTO deliverer = this.personService.getMe();

        deliveryOrder.setDelivererId(deliverer.getId());
        deliveryOrder.setStatus(OrderStatus.STATUS_PICKEDUP);
        this.save(deliveryOrder);

        return this.getDeliveryOrderDTO(deliveryOrder);
    }

    public DeliveryOrderDTO delivered(long orderId) {
        DeliveryOrder deliveryOrder = this.getById(orderId);
        deliveryOrder.setStatus(OrderStatus.STATUS_DELIVERED);
        this.save(deliveryOrder);

        return this.getDeliveryOrderDTO(deliveryOrder);
    }

    public DeliveryOrderDTO cancelled(long orderId) {
        DeliveryOrder deliveryOrder = this.getById(orderId);
        deliveryOrder.setStatus(OrderStatus.STATUS_CANCELLED);
        this.save(deliveryOrder);

        return this.getDeliveryOrderDTO(deliveryOrder);
    }
}