/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ar.edu.unlp.pas.patternsdelivery.domain.model;

import lombok.Data;

/**
 *
 * @author RodAlejo
 */
@Data
public class DeliveryData {
    private Long compradorId;
    private Long vendedorId;
    private Long productId;
    

    public DeliveryData(Long compradorId, Long vendedorId, Long productId) {
        this.compradorId = compradorId;
        this.vendedorId = vendedorId;
        this.productId = productId;
    }
}
