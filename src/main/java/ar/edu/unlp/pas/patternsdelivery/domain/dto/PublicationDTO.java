package ar.edu.unlp.pas.patternsdelivery.domain.dto;

import ar.edu.unlp.pas.patternsdelivery.domain.enums.PublicationStatus;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PublicationDTO {
    Long id;
    String name;
    String description;
    double price;
    String category;
    Long stock;
    PublicationStatus status;
    AddressDTO address;
    PersonDTO seller;
}