package ar.edu.unlp.pas.patternsdelivery.domain.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class SingleLongDTO {
    Long number;
}
