package ar.edu.unlp.pas.patternsdelivery.domain.dto;

import ar.edu.unlp.pas.patternsdelivery.domain.model.status.OrderStatus;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;


@Setter
@Getter
public class DeliveryOrderDTO {

    private Long id;

    private Long quantity;

    @Enumerated(EnumType.STRING)
    private OrderStatus status;

    private PersonDTO deliverer;

    private PersonDTO buyer;

    private AddressDTO address;

    private PublicationDTO publication;
}
