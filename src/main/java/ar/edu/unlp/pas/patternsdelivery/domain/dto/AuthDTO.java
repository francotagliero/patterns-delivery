package ar.edu.unlp.pas.patternsdelivery.domain.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthDTO {
    String requestUri;
}
