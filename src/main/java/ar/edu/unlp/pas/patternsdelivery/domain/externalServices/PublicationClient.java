package ar.edu.unlp.pas.patternsdelivery.domain.externalServices;

import ar.edu.unlp.pas.patternsdelivery.domain.dto.PublicationDTO;
import ar.edu.unlp.pas.patternsdelivery.domain.exceptions.ResourceNotFoundException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Service
public class PublicationClient {

    @Value("${product.uri}")
    private String uri;

    private final ObjectMapper mapper = new ObjectMapper();
    private static final String PUBLICATION = "Publication";
    private final HttpClient httpClient = HttpClient.newHttpClient();

    public PublicationDTO getPublication(Long publicationId) {
        String jwt = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getHeader("authorization");
        HttpRequest request = HttpRequest.newBuilder().setHeader("authorization", jwt)
            .GET().uri(URI.create(uri + "/api/v1/publication/" + publicationId)).build();
        try {
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            return mapper.readValue(response.body(), PublicationDTO.class);
        } catch (IOException | InterruptedException e) {
            throw new ResourceNotFoundException(PUBLICATION, "id", publicationId);
        }
    }

}
