package ar.edu.unlp.pas.patternsdelivery.domain.enums;

public enum PublicationStatus {
    ACTIVE,
    PAUSED,
    FINISHED
}