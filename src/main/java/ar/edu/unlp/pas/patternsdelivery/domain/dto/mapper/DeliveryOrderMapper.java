package ar.edu.unlp.pas.patternsdelivery.domain.dto.mapper;


import ar.edu.unlp.pas.patternsdelivery.domain.dto.DeliveryOrderDTO;
import ar.edu.unlp.pas.patternsdelivery.domain.model.DeliveryOrder;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface DeliveryOrderMapper {

    DeliveryOrderMapper INSTANCE = Mappers.getMapper(DeliveryOrderMapper.class);

    @Mapping(target = "address", ignore = true)
    @Mapping(target = "buyer", ignore = true)
    @Mapping(target = "deliverer", ignore = true)
    DeliveryOrderDTO toDTO(DeliveryOrder deliveryOrder);
}
