/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ar.edu.unlp.pas.patternsdelivery.domain.model;

import ar.edu.unlp.pas.patternsdelivery.domain.dto.DeliveryOrderDTO;
import ar.edu.unlp.pas.patternsdelivery.domain.dto.mapper.DeliveryOrderMapper;
import ar.edu.unlp.pas.patternsdelivery.domain.model.status.OrderStatus;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 *
 * @author RodAlejo
 */
@Entity
@Data
public class DeliveryOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long quantity;
    
    @Enumerated(EnumType.STRING)
    private OrderStatus status;
    
    private Long delivererId;

    @JsonProperty("buyerId")
    private Long buyerId;

    @JsonProperty("addressId")
    private Long addressId;

    @JsonProperty("publicationId")
    @NotNull(message = "Seller is mandatory")
    private Long publicationId;

    public DeliveryOrderDTO toDTO() {
        return DeliveryOrderMapper.INSTANCE.toDTO(this);
    }

}






