package ar.edu.unlp.pas.patternsdelivery.domain.service;

import ar.edu.unlp.pas.patternsdelivery.domain.dto.AddressDTO;
import ar.edu.unlp.pas.patternsdelivery.domain.dto.PersonDTO;
import ar.edu.unlp.pas.patternsdelivery.domain.dto.PublicationDTO;
import ar.edu.unlp.pas.patternsdelivery.domain.externalServices.PublicationClient;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@AllArgsConstructor
@Service
@Transactional
@Slf4j
public class PublicationService {

    private final PublicationClient publicationClient;

    public PublicationDTO getPublication(Long publicationId) {
        return publicationClient.getPublication(publicationId);
    }

}
